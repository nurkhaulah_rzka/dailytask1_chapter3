function luasPersegiPanjang(p, l){
	let hasil = p*l;
	if (hasil <= 50) {
		return 'Hasil luas persegi panjang Anda kurang dari 50 :)';
	} else {
		return 'Luas Persegi Panjang adalah : ' + hasil;
	}
}

module.exports = { luasPersegiPanjang };
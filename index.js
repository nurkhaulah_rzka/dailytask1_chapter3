const readline = require('readline');
const { parse } = require('path');
// Pembagian Fathan
const bagi = require('./operator/pembagian.js');
// Pengurangan Sri
const kurang = require('./operator/pengurangan.js');
// Penjumlahan Fian 
const printPenjumlahan = require('./operator/penjumlahan');
// Perkalian Rima
const kali = require('./operator/perkalian.js');
// PersegiPanjang Rizka
const luas = require('./operator/persegiPanjang.js');


const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('"Pilih Operator + | - | / | x | L " : ', (operator) => {
    var pilihOperator = operator;

    if (pilihOperator == "/") {
        rl.question('Masukan Angka Pertama : ', (angka1) => {
            rl.question('Masukan Angka Kedua : ', (angka2) => {
                const hasilPembagian = bagi.pembagian(angka1, angka2);
                console.log(hasilPembagian);
                rl.close();
            });

        });
    } else if (pilihOperator == "-") {
        rl.question('Masukkan Angka1 :', (angka1) => {
            rl.question('Masukkan Angka2 :', (angka2) => {
                const hasilPengurangan = kurang.pengurangan(angka1, angka2);
                console.log(hasilPengurangan);
                rl.close();
            })
        })
    } else if (pilihOperator == "+") {
        // a = angka1
        let a;
        // b = angka2
        let b;

        rl.question('Angka 1 = ', (angka1) => {
            a = parseFloat(angka1);
            rl.question('Angka 2 = ', (angka2) => {
                b = parseFloat(angka2);
                const hasilPenjumlahan = printPenjumlahan(a, b);
                console.log(hasilPenjumlahan);
                rl.close();
            })
        })
    } else if (pilihOperator == "x") {
        rl.question('Angka 1 : ', (input) => {
            rl.question('Angka 2 : ', (input2) => {
                const hasilPerkalian = kali.perkalian(input, input2);
                console.log(hasilPerkalian);
                rl.close()
            });
        });
    } else if (pilihOperator == "L") {
        rl.question('Panjang : ', (panjang) => {
            rl.question('Lebar : ', (lebar) => {
                const hasilLuas = luas.luasPersegiPanjang(panjang, lebar);
                console.log(hasilLuas);
                rl.close();
            });
        });
    } else {
        console.log("Operator Tidak ada");
        rl.close();
    }

});